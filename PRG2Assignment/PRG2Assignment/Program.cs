﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Student Number: S10227737, S10221938
//Student Name: Nur Muhammad Bin Ismail, Zheng Liang Daryl Lum
//Module Group: P05



namespace PRG2Assignment
{
    class Program
    {

        static void Main(string[] args)
        {
            List<Screening> ScreeningList = new List<Screening>();
            List<Movie> MovieList = new List<Movie>();
            List<Cinema> CinemaList = new List<Cinema>();
            List<Ticket> TicketList = new List<Ticket>();
            List<Order> OrderList = new List<Order>();
            int ordernumber = 0;
            //Main Menu
            while (true)
            {
                Console.WriteLine("---------------- M E N U --------------------");
                Console.WriteLine("\n[1] Load movie and cinema data");
                Console.WriteLine("\n[2] Load screening data");
                Console.WriteLine("\n[3] List all movies");
                Console.WriteLine("\n[4] List movie screenings");
                Console.WriteLine("\n[5] Add a movie screening session");
                Console.WriteLine("\n[6] Delete a movie screening session");
                Console.WriteLine("\n[7] Order movie ticket/s");
                Console.WriteLine("\n[8] Cancel order of ticket");
                Console.WriteLine("\n[0] Exit");
                Console.WriteLine("\n---------------------------------------------");
                Console.WriteLine("\nEnter your option: ");
                int option = Convert.ToInt32(Console.ReadLine());


                if (option == 0)
                {
                    break;
                }

                else if (option == 1)
                {
                    LoadData(ScreeningList, CinemaList, MovieList);
                    Console.WriteLine("Movie and cinema data loaded");

                }
                else if (option == 2)
                {

                    ScreeningData(ScreeningList, CinemaList, MovieList);
                    Console.WriteLine("Screening data loaded");

                }
                else if (option == 3)
                {
                    DisplayMovie(MovieList);
                }
                else if (option == 4)
                {
                    DisplaySceening(ScreeningList, MovieList);
                }
                else if (option == 5)
                {
                    AddScreen(ScreeningList, CinemaList, MovieList);
                }

                else if (option == 6)
                {
                    DeleteScreen(ScreeningList, CinemaList, MovieList);
                }

                else if (option == 7)
                {
                    OrderTickets(ScreeningList, CinemaList, MovieList, TicketList, OrderList, ordernumber);
                    ordernumber += 1;
                }

                else if (option == 8)
                {
                    CancelOrder(ScreeningList, TicketList, OrderList);
                }

                else
                {

                    Console.WriteLine("Invalid option!");

                }



            }

        }
        static void DisplayMovie(List<Movie> MovieList) //this displays the movie
        {



            Console.WriteLine("{0,-30}  {1,-30}  {2,-30}  {3,-30}", "Title", "Duration(mins)", "Classification", "Opening Date");



            foreach (Movie x in MovieList)      //this goes through movie list and prints out
            {


                Console.WriteLine("{0,-30}  {1,-30}  {2,-30}  {3,-30}", x.Title, x.Duration, x.Classification, x.OpeningDate);


            }

        }

        static void DisplaySceening(List<Screening> ScreeningList, List<Movie> MovieList) //this displays all the screenings
        {



            for (int s = 0; s < MovieList.Count; s++)
            {
                Console.WriteLine(MovieList[s].Title);
            }


            Console.WriteLine("Select a movie:");
            string movieselect = Console.ReadLine();


            Console.WriteLine("{0,-20} {1,-25} {2,-20} {3,-20} {4,-20} {5,-10} {6,-20} {7,-20} {8,-20} {9, -20}",
                        "Screening No", "Date Time", "Screening Type", "Cinema Name", "Hall Number", "Capacity", "Movie Title", "Duration(mins)", "Classification", "Opening date");
            for (int i = 0; i < ScreeningList.Count; i++)
            {

                if (ScreeningList[i].M0vie.Title == movieselect)
                {

                    Console.WriteLine("{0,-20} {1,-25} {2,-20} {3,-20} {4,-20} {5,-10} {6,-20} {7,-20} {8,-20} {9, -20}",
                        ScreeningList[i].ScreeningNo, ScreeningList[i].ScreeningDateTime, ScreeningList[i].ScreeningType, ScreeningList[i].Cine.Name, ScreeningList[i].Cine.HallNo,
                        ScreeningList[i].Cine.Capacity, ScreeningList[i].M0vie.Title, ScreeningList[i].M0vie.Duration, ScreeningList[i].M0vie.Classification, ScreeningList[i].M0vie.OpeningDate);
                }
            }

        }

        static void AddScreen(List<Screening> ScreeningList, List<Cinema> CinemaList, List<Movie> MovieList) //this adds a screening session
        {

            DisplaySceening(ScreeningList, MovieList);


            Console.WriteLine("Enter screening type [2D/3D]:  ");
            string screenType = Console.ReadLine();

            DateTime screenDT = DateTime.Now;

            while (true)
            {
                try
                {
                    Console.WriteLine("Enter screening date and time: ");
                    screenDT = Convert.ToDateTime(Console.ReadLine());
                    Movie movie = MovieList[4];
                    DateTime opendate = movie.OpeningDate;
                    if (screenDT < opendate)            //this checks if the screening date time if before opening date time. if yes it will prompt user to choose another date
                    {
                        Console.WriteLine("Please choose another date!");
                        continue;


                    }
                }

                catch (System.FormatException)
                {
                    Console.WriteLine("Enter a valid date/time: ");
                    continue;
                }
                break;
            }


            Console.WriteLine("Cinema halls available:");
            for (int c = 1; c < CinemaList.Count; c++) // to list the cinema halls
            {
                Console.WriteLine(CinemaList[c].Name);
            }

            Console.WriteLine("Select a cinema hall");

            string cinehall = Console.ReadLine();
            Console.WriteLine("Select hall number");
            int hallno = Convert.ToInt32(Console.ReadLine());

            for (int y = 0; y < ScreeningList.Count; y++)
            {
                if (CinemaList[y].HallNo == ScreeningList[y].Cine.HallNo)
                {


                }
            }






        }

        static void DeleteScreen(List<Screening> ScreeningList, List<Cinema> CinemaList, List<Movie> MovieList) //this deletes a screening session
        {
            Console.WriteLine("{0,-20} {1,-25} {2,-20} {3,-20} {4,-20} ",
                       "Screening No", "Cinema Name", "Hall Number", "Capacity", "Movie Title");
            foreach (Screening s in ScreeningList)
            {
                if (s.Cine.Capacity == s.SeatsRemaining)
                {
                    Console.WriteLine("{0,-20} {1,-25} {2,-20} {3,-20} {4,-20} ", s.ScreeningNo, s.Cine.Name, s.Cine.HallNo, s.Cine.Capacity, s.M0vie.Title);
                }

            }
            while (true)
            {
                Console.WriteLine("Select a screening session to delete.");
                int delsession = Convert.ToInt32(Console.ReadLine());

                foreach (Screening s in ScreeningList)
                {

                    if (delsession == s.ScreeningNo)        //this checks whether user input is the same as screening number, if it is it removes it from the list
                    {

                        ScreeningList.Remove(s);
                        Console.WriteLine("Screening removal succesful!");
                        break;
                    }


                    else if (delsession != s.ScreeningNo)

                    {
                        Console.WriteLine("Screening removal unsuccessful.");
                        break;


                    }

                }
                break;
            }




        }




        static void LoadData(List<Screening> ScreeningList, List<Cinema> CinemaList, List<Movie> MovieList) //this loads the data for screening
        {

            string[] csvLines = File.ReadAllLines("Movie.csv");
            string[] csv2Lines = File.ReadAllLines("Cinema.csv");
            for (int i = 1; i < csvLines.Length; i++)
            {

                string[] lines = csvLines[i].Split(",");

                string[] slashlines = lines[2].Split("/");
                List<string> genrelist = new List<string>();
                List<Screening> screentest = new List<Screening>();


                foreach (string y in slashlines)
                {

                    genrelist.Add(y);
                }

                foreach (Screening x in ScreeningList)
                {

                    screentest.Add(x);
                }


                DateTime openDate = Convert.ToDateTime(lines[4]);
                MovieList.Add(new Movie(openDate, lines[3], lines[0], Convert.ToInt32(lines[1]), genrelist));


            }


            for (int i = 1; i < csv2Lines.Length; i++)
            {
                string[] lines2 = csv2Lines[i].Split(',');
                CinemaList.Add(new Cinema(lines2[0], Convert.ToInt32(lines2[1]), Convert.ToInt32(lines2[2])));
            }


        }

        static void ScreeningData(List<Screening> ScreeningList, List<Cinema> CinemaList, List<Movie> MovieList)
        {

            string[] csvLines = File.ReadAllLines("Screening.csv");


            for (int i = 1; i < csvLines.Length; i++)
            {

                string[] lines = csvLines[i].Split(",");


                Cinema chosenCinema = null;
                int capacity = 0;


                foreach (Cinema c in CinemaList)
                {

                    if (c.Name == lines[2] && c.HallNo == Convert.ToInt32(lines[3]))
                    {
                        chosenCinema = c;
                        capacity = chosenCinema.Capacity;
                    }
                }
                Movie m = null;
                foreach (Movie x in MovieList)
                {
                    if (x.Title == lines[4])
                    {
                        m = x;
                    }
                }
                Screening screenobj = new Screening(i + 1001, Convert.ToDateTime(lines[0]), lines[1], chosenCinema, m);

                ScreeningList.Add(screenobj);
                screenobj.SeatsRemaining = capacity;







            }
        }



        static void OrderTickets(List<Screening> ScreeningList, List<Cinema> CinemaList, List<Movie> MovieList, List<Ticket> TicketList, List<Order> OrderList, int ordernumber)//method does the ordering of tickets
        {
            DisplaySceening(ScreeningList, MovieList);
            Console.WriteLine("Select a movie: ");
            string selectmovie = Console.ReadLine();
            Console.WriteLine("Select movie screening number: ");
            int moviescreening = Convert.ToInt32(Console.ReadLine());
            Screening screeningobject = new Screening();
            for (int i = 0; i < ScreeningList.Count; i++)
            {
                if (moviescreening == ScreeningList[i].ScreeningNo)
                {
                    screeningobject = ScreeningList[1];
                }
            }
            Console.WriteLine("Enter total number of tickets to order: ");
            int numbertickets = Convert.ToInt32(Console.ReadLine());

            if (numbertickets > screeningobject.SeatsRemaining)
            {
                Console.WriteLine("Number of tickets is more than the number of available seats.");
            }
            else
            {
                if (screeningobject.M0vie.Classification != "G")
                {
                    Console.WriteLine("Do you meet the movie classification requirements?");
                    string requirements = Console.ReadLine();
                    if (requirements == "No")
                    {
                        Console.WriteLine("You do not meet the requirements.");
                        OrderTickets(ScreeningList, CinemaList, MovieList, TicketList, OrderList, ordernumber);
                    }
                }
            }
            Order orderobj = new Order(ordernumber, DateTime.Now);
            orderobj.Status = "Unpaid";
            double totalprice = 0;
            OrderList.Add(orderobj);
            int ticket = 0;
            while (ticket < numbertickets)
            {

                Console.Write("Type of ticket:Student/Senior Citizen/Adult: ");
                string typeticket = Console.ReadLine();
                while (typeticket != "Adult")
                {
                    if (typeticket == "Student")
                    {
                        Console.Write("Level of study:Primary/Secondary/Tertiary: ");
                        string levelstudy = Console.ReadLine();
                        Ticket studentticket = new Student(screeningobject, levelstudy);
                        TicketList.Add(studentticket);
                        int Sticket = numbertickets;
                        for (int s = 0; s < Sticket; s++)
                        {
                            totalprice += studentticket.CalculatePrice();
                        }
                        break;
                    }
                    else if (typeticket == "Senior Citizen")
                    {
                        Console.Write("Enter your birth year(must be 55 years and above): ");
                        int birthyear = Convert.ToInt32(Console.ReadLine());
                        int age = DateTime.Now.Year - birthyear;
                        if (age < 55)
                        {
                            Console.WriteLine("You need to buy a adult ticket.");
                            typeticket = "Adult";
                        }
                        else
                        {
                            Ticket SeniorCticket = new SeniorCitizen(screeningobject, birthyear);
                            TicketList.Add(SeniorCticket);
                            int scticket = numbertickets;
                            for (int x = 0; x < scticket; x++)
                            {
                                totalprice += SeniorCticket.CalculatePrice();
                            }

                        }
                    }
                }

                if (typeticket == "Adult")
                {
                    Console.Write("Would you like to purchase popcorn for $3[Yes/No]: ");
                    string popcorn = Console.ReadLine();
                    if (popcorn == "Yes")
                    {
                        Ticket ticketyes = new Adult(screeningobject, true);
                        TicketList.Add(ticketyes);
                        int ANoticket = numbertickets;
                        for (int n = 0; n < ANoticket; n++)
                        {
                            totalprice += ticketyes.CalculatePrice() + 3;
                        }

                    }
                    else if (popcorn == "No")
                    {
                        Ticket ticketno = new Adult(screeningobject, false);
                        TicketList.Add(ticketno);
                        int AYesticket = numbertickets;
                        for (int m = 0; m < AYesticket; m++)
                        {
                            totalprice += ticketno.CalculatePrice();
                        }

                    }

                }
                screeningobject.SeatsRemaining -= numbertickets;
                Console.WriteLine("Total price: " + totalprice);
                Console.Write("Press any key to make payment.");
                Console.ReadKey();
                ordernumber += 1;
                Console.WriteLine("Order number: " + ordernumber);
                orderobj.Amount = totalprice;
                orderobj.Status = "Paid";
                break;

            }



        }
        static void CancelOrder(List<Screening> ScreeningList, List<Ticket> TicketList, List<Order> OrderList)//this method cancels the orders
        {
            Console.Write("Please enter order number: ");
            int orderNo = Convert.ToInt32(Console.ReadLine());
            Order cancelorder = new Order();
            for (int i = 0; i < OrderList.Count; i++)
            {
                if (orderNo == OrderList[i].OrderNo)
                {
                    int remaining;
                    for (int r = 0; r < ScreeningList.Count; r++)
                    {
                        remaining = ScreeningList[i].SeatsRemaining - TicketList[i].Screening.SeatsRemaining;
                    }
                    OrderList[i].Status = "Cancelled";
                    Console.WriteLine("Amount has been refunded.");
                    Console.WriteLine("Cancellation successful.");
                }
                else
                {
                    Console.WriteLine("Cancellation unsuccessful");
                }
            }

        }
    }
}


               




    
