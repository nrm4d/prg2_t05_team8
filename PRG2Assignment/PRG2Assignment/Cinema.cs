﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Student Number: S10227737, S10221938
//Student Name: Nur Muhammad Bin Ismail, Zheng Liang Daryl Lum
//Module Group: P05

namespace PRG2Assignment
{
    class Cinema
    {
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private int hallNo;

        public int HallNo
        {
            get { return hallNo; }
            set { hallNo = value; }
        }
        private int capacity;

        public int Capacity
        {
            get { return capacity; }
            set { capacity = value; }
        }

        public Cinema() { }

        public Cinema(string name, int hallNo, int capacity)
        {
            Name = name;
            HallNo = hallNo;
            Capacity = capacity;
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}