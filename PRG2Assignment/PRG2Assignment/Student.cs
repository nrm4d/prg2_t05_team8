﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Student Number: S10227737, S10221938
//Student Name: Nur Muhammad Bin Ismail, Zheng Liang Daryl Lum
//Module Group: P05

namespace PRG2Assignment
{
    class Student : Ticket
    {
        private string levelOfStudy;

        public string LevelOfStudy
        {
            get { return levelOfStudy; }
            set { levelOfStudy = value; }
        }

        public Student() { }
        public Student(Screening screening, string levelOfStudy) : base(screening)
        {
            LevelOfStudy = levelOfStudy;
        }

        public override double CalculatePrice()
        {
            var days = Screening.ScreeningDateTime.DayOfWeek;
            if (this.Screening.ScreeningType == "2D")
            {
                if (this.Screening.ScreeningDateTime <= this.Screening.M0vie.OpeningDate.AddDays(7))
                {
                    if ((days == DayOfWeek.Friday) ||(days == DayOfWeek.Saturday) || (days == DayOfWeek.Sunday))
                    {
                        return 12.50;
                    }
                    else
                    {
                        return 8.50;
                    }

                }
                else
                {
                    if ((days == DayOfWeek.Friday)|| (days == DayOfWeek.Saturday)|| (days == DayOfWeek.Sunday))
                    {
                        return 12.50;
                    }
                    else
                    {
                        return 7.00;
                    }
                }
            }
            else
            {
                if (this.Screening.ScreeningDateTime <= this.Screening.M0vie.OpeningDate.AddDays(7))
                {
                    if ((days == DayOfWeek.Friday)|| (days == DayOfWeek.Saturday)||(days == DayOfWeek.Sunday))
                    {
                        return 14.00;
                    }
                    else
                    {
                        return 11.00;
                    }

                }
                else
                {
                    if ((days == DayOfWeek.Friday)|| (days == DayOfWeek.Saturday)|| (days == DayOfWeek.Sunday))
                    {
                        return 14.00;
                    }
                    else
                    {
                        return 8.00;
                    }
                }
            }
        }

        public override string ToString()
        {
            return base.ToString();
        }

    }
}
