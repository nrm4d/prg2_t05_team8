﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Student Number: S10227737, S10221938
//Student Name: Nur Muhammad Bin Ismail, Zheng Liang Daryl Lum
//Module Group: P05

namespace PRG2Assignment
{
    class Order 
    {
        private int orderNo;

        public int OrderNo
        {
            get { return orderNo; }
            set { orderNo = value; }
        }
        private DateTime orderDateTime;     

        public DateTime OrderDateTime
        {
            get { return orderDateTime; }
            set { orderDateTime = value; }
        }

        private double amount;

        public double Amount
        {
            get { return amount; }
            set { amount = value; }
        }
        private string status;

        public string Status
        {
            get { return status; }
            set { status = value; }
        }
        private List<Order> ticketList; 

        public List<Order> TicketList
        {
            get { return ticketList; }
            set { ticketList = value; }
        }

        public Order() { }
        
        public Order(int orderNo, DateTime orderDateTime)
        {
            OrderNo = orderNo;
            OrderDateTime = orderDateTime;
        }

        public void AddTicket(Order addTick)
        {
            ticketList.Add(addTick);
        }

        public void GetTicketList(List<Order> TicketList)
        {

        }
        public override string ToString()
        {
            return base.ToString();
        }
    }
}
