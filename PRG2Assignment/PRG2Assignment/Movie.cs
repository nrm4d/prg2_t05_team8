﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Student Number: S10227737, S10221938
//Student Name: Nur Muhammad Bin Ismail, Zheng Liang Daryl Lum
//Module Group: P05

namespace PRG2Assignment
{
    class Movie
    {
        private string title;

        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        private int duration;

        public int Duration
        {
            get { return duration; }
            set { duration = value; }
        }
        private string classification;

        public string Classification
        {
            get { return classification; }
            set { classification = value; }
        }
        private DateTime openingDate;

        public DateTime OpeningDate

        {
            get { return openingDate; }
            set { openingDate = value; }
        }

        private List<string> genreList;

        public List<string> GenreList
        {
            get { return genreList; }
            set { genreList = value; }
        }

        private List<Movie> screeningList;

        public List<Movie> ScreeningList
        {
            get { return screeningList; }
            set { screeningList = value; }
        }


        public Movie() { }
        public Movie(DateTime openingDate, string classification, string title, int duration, List<string> genreList)
        {
            Title = title;
            Duration = duration;
            GenreList = genreList;
            Classification = classification;
            OpeningDate = openingDate;

        }



        /*public void AddScreening(Screening addScreen)
        {
            ScreeningList.Add(addScreen);
        }*/

        public void AddGenre(string G)
        {
            GenreList.Add(G);
        }

        public override string ToString()
        {
            return base.ToString();
        }







    }
}