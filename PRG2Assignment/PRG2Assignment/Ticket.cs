﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Student Number: S10227737, S10221938
//Student Name: Nur Muhammad Bin Ismail, Zheng Liang Daryl Lum
//Module Group: P05

namespace PRG2Assignment
{
    abstract class Ticket
    {
        

        public Screening Screening { get; set; }
        
        public Ticket() { }
        public Ticket(Screening screening)
        {
            Screening = screening;

        }

        public abstract double CalculatePrice();
        
            
        
        public override string ToString()
        {
            return base.ToString();
        }
    }
}
