﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Student Number: S10227737, S10221938
//Student Name: Nur Muhammad Bin Ismail, Zheng Liang Daryl Lum
//Module Group: P05

namespace PRG2Assignment
{
    class Screening : Cinema
    {
        private int screeningNo;

        public int ScreeningNo
        {
            get { return screeningNo; }
            set { screeningNo = value; }
        }

        public DateTime ScreeningDateTime { get; set; }
        private string screeningType;

        public string ScreeningType
        {
            get { return screeningType; }
            set { screeningType = value; }
        }
        private int seatsRemaining;

        public int SeatsRemaining
        {
            get { return seatsRemaining; }
            set { seatsRemaining = value; }
        }

        private Cinema cinema;

        public Cinema Cine
        {
            get { return cinema; }
            set { cinema = value; }
        }
        private Movie movie;

        public Movie M0vie
        {
            get { return movie; }
            set { movie = value; }
        }
        public Screening() { }
        public Screening(int screeningNo, DateTime screeningDateTime, string screeningType, Cinema cinema, Movie movie)
        {
            ScreeningNo = screeningNo;
            ScreeningDateTime = screeningDateTime;
            ScreeningType = screeningType;
            Cine = cinema;
            M0vie = movie;
        }

        public override string ToString()
        {
            return base.ToString();
        }



    }
}